jQuery(document).ready(function($) {

  //menu toggle
  $('.nav-toggle').click(function(e) {
    e.preventDefault();
    $('.nav--mobile').toggleClass('is-open');
    $('body').toggleClass('is-fixed');
  });


  //menu toggle
  if ($(window).width() < 768) {
    $('.nav__item').click(function(e) {
      e.preventDefault();
      $('.nav--mobile').toggleClass('is-open');
      $('body').toggleClass('is-fixed');
    });
  }


  //Smooth scroll
  $('a[href^="#"]').on('click',function (e) {
    e.preventDefault();

    var target = this.hash;
    var $target = $(target);

    $('html, body').stop().animate({
      'scrollTop': $target.offset().top
    }, 900, 'swing', function () {
      window.location.hash = target;
    });
  }); 

  //owl slider/carousel
  var $owl = $(".slider__track");

  $owl.each(function() {
    $(this).children().length > 1;

      $(this).owlCarousel({
          loop: false,
          items: 1,
          autoplay: true,
          // nav: true,
          autplaySpeed: 11000,
          autoplayTimeout: 10000,
          smartSpeed: 250,
          smartSpeed: 2200,
          navSpeed: 2200
          // navText : ["<i class='fa fa-arrow-left' aria-hidden='true'></i>", "<i class='fa fa-arrow-right' aria-hidden='true'></i>"]
      });
    });

});
