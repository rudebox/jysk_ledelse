<?php get_template_part('parts/header'); ?>

<main>

  <?php get_template_part('parts/content', 'layouts'); ?>

  <?php 

	$location = get_field('google_maps', 'options');

	if( !empty($location) ):
	?>
		<div class="acf-map">
			<div class="marker" data-lat="<?php echo $location['lat']; ?>" data-lng="<?php echo $location['lng']; ?>"></div>
		</div>
	<?php endif; ?>

</main>

<?php get_template_part('parts/footer'); ?>
