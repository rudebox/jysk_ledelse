<?php

/* Do not remove this line. */
require_once('includes/scratch.php');
require_once('lib/theme-dependencies.php');


//google map api key
define( 'GOOGLE_MAPS_KEY', 'AIzaSyBYFRopufYXVtMey2U0h6wiRB78Q7y3Jo0' );


/*
 * adds all meta information to the <head> element for us.
 */

function scratch_meta() { ?>

  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <meta name="description" content="<?php bloginfo('description'); ?>">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <link rel="apple-touch-icon" href="/apple-touch-icon.png">
  <!-- Place favicon.ico in the root directory -->

<?php }

add_action('wp_head', 'scratch_meta');

/* Theme CSS */

function theme_styles() {

  wp_enqueue_style( 'normalize', 'https://cdnjs.cloudflare.com/ajax/libs/normalize/8.0.0/normalize.min.css', false, null );

  wp_enqueue_style( 'fontawesome', 'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css', false, null );

  wp_register_style( 'scratch-main', get_template_directory_uri() . '/assets/css/master.css', false, filemtime(dirname(__FILE__) . '/assets/css/master.css') );
  wp_enqueue_style( 'scratch-main' );

}

add_action( 'wp_enqueue_scripts', 'theme_styles' );

/* Theme JavaScript */

function theme_js() {

  // wp_enqueue_script( 'modernizr', get_template_directory_uri() . '/assets/js/vendor/modernizr-2.8.3.min.js', false, false, false );

  wp_register_script( 'scratch-main-concat', get_template_directory_uri() . '/assets/js/concat/main.js', array('jquery'), filemtime(dirname(__FILE__) . '/assets/js/concat/main.js'), true ); 

  wp_register_script( 'scratch-main-min', get_template_directory_uri() . '/assets/js/build/main.min.js', array('jquery'), filemtime(dirname(__FILE__) . '/assets/js/build/main.min.js'), true );

  /* FOR DEVELOPMENT */
  // wp_enqueue_script( 'scratch-main-concat' );

  /* FOR PRODUCTION */
  wp_enqueue_script( 'scratch-main-min' );

}

add_action( 'wp_enqueue_scripts', 'theme_js' );


/* Enable ACF Options Pages */


if( function_exists('acf_add_options_page') ) {
  
  acf_add_options_page(array(
    'page_title'  => 'Globalt indhold',
    'menu_title'  => 'Globalt indhold',
    'menu_slug'   => 'global-content',
    'capability'  => 'edit_posts',
    'redirect'    => false
  ));
  
  acf_add_options_sub_page(array(
    'page_title'  => 'Header',
    'menu_title'  => 'Header',
    'parent_slug' => 'global-content',
  ));

  acf_add_options_sub_page(array(
    'page_title'  => 'Sidebar',
    'menu_title'  => 'Sidebar',
    'parent_slug' => 'global-content',
  ));
  
  acf_add_options_sub_page(array(
    'page_title'  => 'Footer',
    'menu_title'  => 'Footer',
    'parent_slug' => 'global-content',
  ));
}

/* Enable Featured Image */

add_theme_support( 'post-thumbnails' );

/* Enable Custom Menus */

add_theme_support( 'menus' );

register_nav_menus(
  array(
    'scratch-main-nav' => __( 'Main Nav', 'scratch' )   // main nav in header
  )
);

function scratch_main_nav() {
  // display the wp3 menu if available
  wp_nav_menu(array(
    'container' => false, // remove nav container
    'container_class' => '', // class of container (should you choose to use it)
    'menu' => __( 'Main Nav', 'scratch' ), // nav name
    'menu_class' => 'nav__menu', // adding custom nav class
    'theme_location' => 'scratch-main-nav', // where it's located in the theme
    'before' => '', // before the menu
    'after' => '', // after the menu
    'link_before' => '', // before each link
    'link_after' => '', // after each link
    'depth' => 0,    // fallback function
    'walker' => new Clean_Walker_Nav () //remove wordpress walker
  ));
} /* end scratch main nav */


/**
 * Google Maps API key for ACF
 * @since 2.0.0
 **/

if ( defined( 'GOOGLE_MAPS_KEY' ) && GOOGLE_MAPS_KEY !== null ) {
  function add_google_maps_key_to_acf( $api ) {
      $api['key'] = GOOGLE_MAPS_KEY;
      return $api;
  }

  add_filter('acf/fields/google_map/api', 'add_google_maps_key_to_acf');
}

wp_enqueue_script( 'google-maps', 'https://maps.googleapis.com/maps/api/js?key=' . GOOGLE_MAPS_KEY, array('jquery'), null, true );


//make gf submit input to button
function gf_make_submit_input_into_a_button_element($button_input, $form) {

  //save attribute string to $button_match[1]
  preg_match("/<input([^\/>]*)(\s\/)*>/", $button_input, $button_match);

  //remove value attribute
  $button_atts = str_replace("value='".$form['button']['text']."' ", "", $button_match[1]);

  return '<button '.$button_atts.'><span>'.$form['button']['text'].'</span></button>';
}
add_filter('gform_submit_button', 'gf_make_submit_input_into_a_button_element', 10, 2);



add_filter("gform_init_scripts_footer", "init_scripts");
  function init_scripts() {
  return true;
}


/* Place custom functions below here. */

/* Don't delete this closing tag. */
?>
