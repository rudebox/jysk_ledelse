<footer class="footer" id="footer">
	<div class="wrap hpad clearfix">
		<div class="row">
			<?php 
				if (have_rows('footer_columns', 'options') ) : while (have_rows('footer_columns', 'options') ) :
					the_row();
				$title = get_sub_field('column_title');
				$text = get_sub_field('column_text');
			 ?>

			 <div class="col-sm-4 footer__item">
			 	<h4 class="footer__title"><?php echo esc_html($title); ?></h4>
				<?php echo $text; ?>
			 </div>

			<?php endwhile; endif; ?>
		</div>
	</div>
</footer>

<?php wp_footer(); ?>

<?php if(!is_preview()): ?>
<?php $ga_id = get_field('ga_id', 'options'); ?>
<!-- Google Analytics: change UA-XXXXX-Y to be your site's ID. -->
<!-- <script>
  window.ga=function(){ga.q.push(arguments)};ga.q=[];ga.l=+new Date;
  ga('create','<?php echo $ga_id; ?>','auto');ga('send','pageview')
</script>
<script src="https://www.google-analytics.com/analytics.js" async defer></script> -->
<?php endif; ?>

</body>
</html>
