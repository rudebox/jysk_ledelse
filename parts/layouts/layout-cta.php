<?php
/**
 * Description: Lionlab text_img field group
 *
 * @package Lionlab
 * @subpackage Lionlab
 * @since Version 1.0
 * @author Kaspar Rudbech
*/

//section settings
$bg = get_sub_field('bg');
$margin = get_sub_field('margin');
$anchor = get_sub_field('anchor');

$title = get_sub_field('title');
$phone = get_sub_field('phone_link');
$phone_text = get_sub_field('phone_link_text');
$img = get_sub_field('img');

?>

<section id="<?php echo esc_html($anchor); ?>" class="cta padding--<?php echo esc_html($margin); ?>" style="background-image: url(<?php echo $img['url']; ?>);">
	<div class="wrap hpad center cta__container padding--bottom">
		<h2 class="cta__title"><?php echo esc_html($title); ?></h2>
		<a class="btn btn--gradient cta__btn" href="tel:<?php echo get_formatted_phone(esc_html($phone)); ?>"><?php echo esc_html($phone_text); ?></a>

		<div class="row cta__row">
			<div class="col-sm-8 cta__form">
				<?php gravity_form( 1, $display_title = false, $display_description = false, $display_inactive = false, $field_values = null, $ajax = false, 2, $echo = true ); ?>
			</div>
		</div>
	</div>
</section>