<?php
/**
 * Description: Lionlab references repeater field group
 *
 * @package Lionlab
 * @subpackage Lionlab
 * @since Version 1.0
 * @author Kaspar Rudbech
*/

//section settings
$bg = get_sub_field('bg');
$margin = get_sub_field('margin');
$anchor = get_sub_field('anchor');
$title = get_sub_field('header');


if (have_rows('reference') ) : 
?>

<section id="<?php echo esc_html($anchor); ?>" class="references <?php echo esc_html($bg); ?>--bg padding--<?php echo esc_html($margin); ?>">
	<div class="wrap hpad">
		<h2 class="references__title"><?php echo $title; ?></h2>
		<div class="row">
			<?php 
				while (have_rows('reference') ) : the_row();

				$name = get_sub_field('reference_name');
			 ?>

			 	<div class="col-sm-4">
			 		<?php echo $name; ?>	
			 	</div>

			<?php endwhile; ?>

		</div>
	</div>
</section>
<?php endif; ?>