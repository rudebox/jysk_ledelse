<?php
/**
 * Description: Lionlab text_img field group
 *
 * @package Lionlab
 * @subpackage Lionlab
 * @since Version 1.0
 * @author Kaspar Rudbech
*/

//section settings
$bg = get_sub_field('bg');
$margin = get_sub_field('margin');
$anchor = get_sub_field('anchor');

$title = get_sub_field('title');
$text = get_sub_field('text');
$img = get_sub_field('img');
$position = get_sub_field('position');

//get inline svg
$image_id = $img['ID'];

//position
if ($position === 'left') {
	$push = 'col-sm-push-6';
	$pull = 'col-sm-pull-6';
}

else {
	$pull = '';
	$push = '';
}

?>

<section id="<?php echo esc_html($anchor); ?>" class="text_img <?php echo esc_html($bg); ?>--bg padding--<?php echo esc_html($margin); ?>">
	<div class="wrap hpad">
		<div class="row">
			<div class="col-sm-6 text_img__textbox <?php echo esc_html($push); ?>">
				<h2 class="text_img__title"><?php echo esc_html($title); ?></h2>
				<?php echo $text; ?>
			</div>

			<div class="col-sm-6 text_img__img <?php echo esc_html($pull); ?> <?php echo esc_html($position); ?>">
				<?php echo zen_inline_if_svg( $image_id, 'medium' ); ?>
			</div>
		</div>
	</div>
</section>